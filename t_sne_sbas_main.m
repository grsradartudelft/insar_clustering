% Takes output files from JIHG joint SBAS processing
% Runs t-SNE on the temporal 

clear
clc
close all

project = 'zegveld';
run     = '2';
track   = 'dsc_t110';

filterPS   = 1; % (0): no      (1): Filter ps from sbas result
saveOutput = 0; % (0): no      (1): Save output to csv file

sigma_threshold = 2;
gamma_pta_threshold = 0; % Set to 0 to not filter by coherence

%% Load output files 
result_folder = ['/Users/pconroy/phd/projects/' project '/output/' project '_joint_run' run '/'];

addpath(genpath(result_folder))
addpath(genpath('/Users/pconroy/phd/code/floris/jihg_modules'))

load('results_p1_zegveld_joint_run2.mat')
load('results_p2_zegveld_joint_run2.mat')
load('results_p3_zegveld_joint_run2.mat')
load('results_p4_zegveld_joint_run2.mat')

im = imread('/Users/pconroy/phd/projects/zegveld/AOI/zegveld_AOI.png');
%% Get stack parameters
% Note that for joint processing most of the metadata is contained in "VAR"
% vertDefSBAS      = table2array(sortrows(TSR_SBAS));
vertDefEMI        = table2array(TSR_SBAS(1:177,:)); %Not actually SBAS...
vertDefEMI(:,end) = [];
vertDefRCR         = table2array(TSR_6d);
vertDefRCR(:,end)  = [];    
los2vert           = table2array(VAR(:,end));
inc                = acosd(1./los2vert);
coherence          = table2array(COH(1:177,:));
coherence(:,end)   = [];

% TSR.Time has 2 sets of dates, we only use the 1st here
dates = TSR_SBAS.Time(1:177);
yrs   = years(dates(end)-dates(1));

winLat = table2array(VAR(:,5));
winLon = table2array(VAR(:,4));
nwindows = length(winLat);
winIdx   = (1:length(winLat))';
nEpochs  = length(dates);

for i = 1:length(dates)
    colNames{i} = [datestr(dates(i),'yyyymmdd')];
end
stMatrixTable = array2table(vertDefEMI','VariableNames',colNames,'RowNames',cellstr(num2str(winIdx)));
coherenceTable = array2table(coherence','VariableNames',colNames,'RowNames',cellstr(num2str(winIdx)));


%% setup t-SNE
parameter = vertDefEMI;

% Don't include nans (vertDefRCR already has PS points filtered out)
if filterPS == 1 
    vertDefRCR(:,VAR.gamma_pta < gamma_pta_threshold) = nan;
    keep = ind2sub(size(vertDefRCR),~isnan(vertDefRCR));
    keep = find(keep(1,:));
else
    parameter(:,VAR.gamma_pta < gamma_pta_threshold) = nan;
    keep = ind2sub(size(parameter),~isnan(parameter));
    keep = find(keep(1,:));
end

% Apply RPN Correction
parameter = correct_RPN(parameter,0);
metadata.rpnCorrection = 1;

tSNEin = parameter(:,keep);
tSNEin = tSNEin - mean(tSNEin,1);

%% t-SNE
cloud = tsne(tSNEin');

figure
gscatter(cloud(:,1),cloud(:,2))
title('t-SNE Ouput Space')
legend(gca,'off');
set(gca,'FontSize',16)
set(gcf,'color','w')
axis off

%% Clustering
close all

% Cluster t-SNE results into functional groups using dbscan
[clusters, corepts] = dbscan(cloud,2.2,21);%2.4,28);
% [clusters, corepts] = dbscan(cloud,2.4,40);

% Create a list of cluster ids for each DS point incl. masked points
clusters_full       = nan(length(winLat),1);
clusters_full(keep) = clusters;

% Clustering results (debugging)
figure; hold on
gscatter(cloud(clusters==-1,1),cloud(clusters==-1,2),clusters(clusters==-1),'k','o')
gscatter(cloud(clusters~=-1,1),cloud(clusters~=-1,2),clusters(clusters~=-1))
title('Clustering in t-SNE Ouput Space')
legend(gca,'off');
set(gca,'FontSize',16)
set(gcf,'color','w')
axis off

%% Estimate Functional Model

for i = 1:max(clusters)
    
    % Prepare functional model
%     l2v_mat  = repmat(los2vert(clusters_full==i)',177,1);
%     defo     = mean(tSNEin(:,clusters==i)./l2v_mat,2);
    defo     = mean(tSNEin(:,clusters==i),2);
    t        = (1:length(dates))';
    
    % Initial estimates
    offset   = defo(1);
    range    = max(defo)-min(defo);
    slope    = (defo(end)-defo(1))/length(dates);
    amp      = range/10;
    freq     = 6/365; % 1 year in 6-day epochs
    phase    = pi/2;
    
    model    = @(theta,t) theta(1) + theta(2)*t + theta(3)*sin( 2*pi*theta(4)*t + theta(5) );
    cost     = @(theta) mean((model(theta,t)-defo).^2);
    sol(i,:) = fminunc(cost,[offset,slope,amp,freq,phase]);
end

%% Extract linear deformation from model
linDefGroups = sol(:,2)*177/yrs;
linDef       = zeros(size(winLat));

for i = 1:max(clusters)
    linDef(clusters_full==i) = linDefGroups(i);
end

%% debugging
% figure; hold on
% plot(dates,defo,'.')
% plot(dates,model(sol(i,:),t))
% plot(dates,sol(i,2).*t+sol(i,1))
% grid on

%% Cluster histograms - calculate std of the spreads
% for i = 1:max(clusters)
%     % Calculate the euclidean distances of the points from the mean
%     spread = vecnorm(abs(cloud(clusters==i,:)) - mean(abs(cloud(clusters==i,:))),2,2);
%     sigma(i) = std(spread);
%     
%     figure
%     histogram(spread)
%     title(['Cluster ' num2str(i) ' \sigma = ' num2str(sigma(i))])
% end

%% Recluster groups with sigma > threshold

% i = 1;
% while any(sigma > sigma_threshold)
%     if sigma(i) > sigma_threshold
%         subcloud = cloud(clusters == i,:);
%         newGroups = dbscan(subcloud,1.8,15);
%     end
%     i=i+1;
% end

%% plot clustered time series


for i = 1:max(clusters)
    figure('Renderer', 'painters', 'Position', [100 300 1500 400]);
    sgtitle(['Cluster ' num2str(i)])
%     sgtitle(['Cluster ' num2str(i) ' (\sigma = ' num2str(sigma(i)) ')'])
    set(gcf,'Color','w')
    set(gca,'FontSize',16)
    
    subplot(1,3,1);
    hold on
    gscatter(cloud(clusters==-1,1),cloud(clusters==-1,2),clusters(clusters==-1),'k','o')
    gscatter(cloud(clusters~=i & clusters~=-1,1),cloud(clusters~=i & clusters~=-1,2),clusters(clusters~=i & clusters~=-1),'b')
    gscatter(cloud(clusters==i,1),cloud(clusters==i,2),clusters(clusters==i),'r')
    hold off
    title('t-SNE cluster plot')
    legend('Outliers','Other clusters','Current cluster');
    set(gca,'FontSize',16)
    set(gcf,'color','w')
    axis off
    
    subplot(1,3,2); hold on
    image('CData',flipud(im),'XData',[min(winLon), max(winLon)],'YData',[min(winLat), max(winLat)]);
    clusterSelection = nan(size(clusters_full));
    clusterSelection(clusters_full==i) = i;
    gscatter(winLon,winLat,clusterSelection,'r','.',10)
    hold off
    xlabel('Longitude (deg)')
    ylabel('Latitude (deg)')
    title('Aerial View')
    set(gca,'FontSize',16)
    axis tight
    legend('off')
    
    subplot(1,3,3);
    hold on;
    plotVar = tSNEin(:,clusters==i);
%     plotVar = plotVar./repmat(los2vert(clusters_full==i)',177,1);
    plot(dates,plotVar(:,1:end-1),'.-')
    plot(dates,model(sol(i,:),t),'k','LineWidth',2)
    hold off
    xlabel('Epoch')
    %             ylabel('LOS displacement projected onto vertical (mm)')
    ylabel('LOS displacement (mm)')
    grid on
    title('Functional Group Time-Series')
    set(gca,'FontSize',16)
    %             saveas(gcf,['zegveld_cluster_' num2str(i) '.png'])
end



%% Export to CSV

if saveOutput == 1
    output_filename   = [project '_clustering_' track '_' date '.csv'];
    metadata.dataFile = output_filename;
    metadata.nwindows = nwindows;
    metadata.nEpochs  = nEpochs;
    metadata.stMatrixType = 'LOS';
    
    write_contextual_csv(dates,winIdx,winLat,winLon,inc,linDef,vertDefEMI'./repmat(los2vert,1,nEpochs),clusters_full,metadata)
end