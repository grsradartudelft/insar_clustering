# InSAR clustering toolbox

The InSAR clustering toolbox provides tools for the clustering of InSAR data. The concepts and methodologies are described in 

Van de Kerkhof, B., Pankratius, V., Chang, L., Van Swol, R., & Hanssen, R. F. (2020). Individual Scatterer Model Learning for Satellite Interferometry. IEEE Transactions on Geoscience and Remote Sensing, 58(2), 1273-1280. [8878002]. https://doi.org/10.1109/TGRS.2019.2945370 (Open access).

The toolbox is currently based on Matlab.

